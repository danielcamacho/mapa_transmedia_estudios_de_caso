$(document).ready(function() {
    
    /*----FIG 1 y 2 SECTOR SIZE ESTABLISHMENTS------*/

    $("#number-of-employees .sector-bar").each(function(){
        var percentage = (parseFloat($(this).find('.sector-size').first().text())/200000)*100;
        $(this).find('.pink-bar').animate({
            width: percentage + "%"
        }, 200);
    });
    $("#number-of-establishments .sector-bar").each(function(){
        var percentage = (parseFloat($(this).find('.sector-size').first().text())/30000)*100;
        $(this).find('.pink-bar').animate({
            width: percentage + "%"
        }, 200);
    });

    /*----END FIG 1 y 2 SECTOR SIZE ESTABLISHMENTS----*/




    /*----FIG 4 ESTABLISHMENTS AND EMPLOYEES ------*/

    $('.bar-section').each(function(){
        var total = parseFloat($(this).parents('.bar-section-container').first().attr('data-total'));
        var percentage = (parseFloat($(this).attr('data-size'))/total)*100;
        $(this).animate({
            width: percentage + "%"
        }, 200);
    });

    $("#fig4 .option").click(function(){
        $("#fig4 .option").removeClass('active');
        $(this).addClass("active");

        $(".hide-f4").removeClass("active");
        $("#"+$(this).attr("data-target")).addClass("active");
    });

    /*================== para tooltips figs 4 y 10 ==================*/

    $('.state-bar').mouseover(function(ev){
        //console.log($(this));
        getEstadoData($(this));
    });

    $('.state-bar').mouseout(function(ev){
        //console.log($(this));
        clearEstadoTooltip($(this));
    });


    function getEstadoData(elem)
    {
        estado = elem.prev().text();
        children = elem[0].children;
        figClass = elem.parent().parent().parent()[0].id;
        categorias4 = ['Música y artes interpretativas', 'Editorial', 'Publicidad y marketing', 'Arquitectura', 'Artesanía', 'Librerías y museos', 'Software', 'Cine, Radio y Televisión', 'Diseño'];
        categorias10 = ['Otros sectores', 'Software', 'Diseño', 'Videojuegos, inmersión y apps', 'Publicidad y marketing', 'Freelance'];
        labels = [];
        for (x = 0; x < children.length; x++)
        {
            classList = children[x].className.split(' ');
            labElem = {};
            if ((figClass == 'fig4-establishments') || (figClass == 'fig4-employees'))
                labElem.cat = categorias4[x];
            else if ((figClass == 'fig10-establishments') || (figClass == 'fig10-participants'))
                labElem.cat = categorias10[x];
            labElem.val = children[x].dataset.size;
            labElem.colorClass = classList[0];
            labels.push(labElem);
        }
        style = 'style="position: absolute; background: black; color: white; width:285px; height:auto; top: 120%; left: 0; z-index: 200; padding: 10px; border-radius: 5px;"';
        tooltipHtml = '<div class="figs4-10Tooltip" '+style+'>';
        tooltipHtml += '<p style="font-size: small; line-height: 15px;"><strong>'+estado+'</strong></p>';
        for(x = 0; x < labels.length; x++)
        {
            cuadritoStyle = 'style="width: 15px; height: 15px; border: 1px solid white; display: inline-flex; position: relative; margin-right: 5px;"';
            tooltipHtml += '<p style="font-size: small; line-height: 10px;">';
            tooltipHtml += '<span class="fig6-10cuadritoToolTip '+labels[x].colorClass+'" '+cuadritoStyle+'></span>';
            tooltipHtml += labels[x].cat+': '+labels[x].val+'</p>';
        }
        tooltipHtml += '</div>';
        elem.append(tooltipHtml);
    }

    function clearEstadoTooltip(elem)
    {
        elem.children('.figs4-10Tooltip').remove();
    }

    /*================================================================*/


    /*----FIG 10 MEETUPS ------*/

    $("#fig10 .option").click(function(){
        $("#fig10 .option").removeClass('active');
        $(this).addClass("active");

        $(".hide-f10").removeClass("active");
        $("#"+$(this).attr("data-target")).addClass("active");
    });
    /*----END FIG 10 MEETUPS ------*/
});