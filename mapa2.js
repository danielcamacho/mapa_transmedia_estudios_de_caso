var state_data, stateChart;
$(document).ready(function() {

	var range = getSectorRange(data_set, selected_sector);
    mapSector(data_set, selected_sector, range);

    getCountryAverage();

    $('.cbx').click(function(){
        if($(this).prop('checked')){
            selected_sector = $(this).attr('data-sector');
            var range = getSectorRange(data_set, selected_sector);
            mapSector(data_set, selected_sector, range);
        }
        $('.cbx').prop('checked',false);
        $(this).prop('checked',true);
    });

    $('.data-set').click(function(){
        $('.data-set').removeClass("active");
        $(this).addClass("active");

        var selected_data = $(this).attr("data-set");

        switch(selected_data){
            case "concentration_creative_business":
                data_set = concentration_creative_business;
                break;
            case "concentration_creative_employment":
                data_set = concentration_creative_employment;
                break;
        }
        
        var range = getSectorRange(data_set, selected_sector);

        mapSector(data_set, selected_sector, range);
    });


    var ctx = document.getElementById("state-chart").getContext("2d");

    var countryData = getCountryAverage();

    var countryNumbers = getCountryNumbers();

    $("#business-count").text(countryNumbers[0].toLocaleString('en-US'));
    $("#employee-count").text(countryNumbers[1].toLocaleString('en-US'));

    state_data = {
        labels: ["Publicidad y marketing","Arquitectura","Artesanías","Diseño","Cine, radio y televisión","Bibliotecas y museos","Artes escenicas","Editorial","Software"],
        datasets: [{
            label: "Promedio nacional",
            backgroundColor: "#bbbbbb44",
            borderColor: "#bbbbbb",
            pointBackgroundColor: "#bbbbbb",
            borderCapStyle: "round",
            borderJoinStyle: "round",
            borderWidth: 0.1,
            pointRadius: 4,
            data: countryData
        }]
    };

    /*state_data = {
        labels: ["Publicidad y marketing","Arquitectura","Artesanías","Diseño","Cine, radio y televisión","Bibliotecas y museos","Artes escenicas","Editorial","Software"],
        datasets: [{
            label: "Especialización creativa",
            backgroundColor: "#ee416a44",
            borderColor: "#ee416a",
            pointBackgroundColor: "#ee416a",
            borderCapStyle: "round",
            borderJoinStyle: "round",
            borderWidth: 0.1,
            pointRadius: 4,
            data: countryData
        }]
    };*/

    //Mapa 2
    var options = {
        scale: {
            ticks: {
                display: false,
                maxTicksLimit: 6,
                min: 0,
                max: 3
            },
            gridLines: {
                circular: true,
                color: "#eeeeee"
            },
            angleLines: {
                color: "#eeeeee"
            }
        },
        legend: {
            display: true,
            position: "bottom"
        },
        tooltips: {
            enabled: true
        }
    };

    stateChart = new Chart(ctx, {
        type: 'radar',
        data: state_data,
        options: options
    });


  
    $("#creative-mapa-mexico .state").click(function() {
        $(".state").removeClass("selected");
        $(this).addClass("selected");
        $("#creative-mapa-mexico .state").css('fill','rgb(250, 175, 64)');
        $(this).css('fill','rgb(255, 108, 2)');
        var stateId = $(this).attr("id").replace("creative-","");
        var name = stateId.replace(new RegExp('-', 'g')," ");
        $("#state-title").text(name);

        var data = getStateData(creative_specialisation_map, stateId);

        var chart_options = {
            label: name.replace(/\b\w/g, function(l){ return l.toUpperCase() }),
            backgroundColor: "#ff6c0244",//
            borderColor: "#ff6c02",
            pointBackgroundColor: "#ff6c02",
            borderCapStyle: "round",
            borderJoinStyle: "round",
            borderWidth: 0.1,
            pointRadius: 4,
            data: data
        }

        state_data.datasets[1] = chart_options;

        stateChart.data = state_data;
        stateChart.update();

        var state = getStateNumbers(stateId);

        $("#business-count").text(state.business_estimate.toLocaleString('en-US'));
        $("#employee-count").text(state.employment_estimate.toLocaleString('en-US'));

    });

    function map (num, in_min, in_max, out_min, out_max) {
	  return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	function getCountryAverage(){
        var return_data = [0,0,0,0,0,0,0,0,0];
        creative_specialisation_map.forEach(function(state){
            return_data[0] += state.advertising_marketing;
            return_data[1] += state.architecture;
            return_data[2] += state.crafts;
            return_data[3] += state.design;
            return_data[4] += state.film_radio_tv;
            return_data[5] += state.libraries_museums;
            return_data[6] += state.music_performing_arts;
            return_data[7] += state.publishing;
            return_data[8] += state.software;
        });
        for(var i=0; i<return_data.length; i++){
            return_data[i] = return_data[i]/32;
        }

        return return_data;
    }

    function getStateData(data, stateId){
		var return_data;
		data.forEach(function(state){
			if(state.state == stateId){
				return_data = [state.advertising_marketing, state.architecture, state.crafts, state.design, state.film_radio_tv, state.libraries_museums, state.music_performing_arts, state.publishing, state.software];
			}
		});

		return return_data;
	}

	function getStateNumbers(stateId){
		var stateInfo;
		number_creative_business.forEach(function(state) {
			if(state.state == stateId) {
				stateInfo = state;
			}
		});
		return stateInfo;
	}

	function getSectorRange(data, sector){
		var min = 999999,
		max = 0;

		data.forEach(function(state){
			if(state[sector]>max){
				max = state[sector];
			}
			if(state[sector]<min){
				min = state[sector];
			}
		});

		$('#top-data').text(max);
		$('#bottom-data').text(min);

		return [min, max];
	}

	function mapSector(data, sector, range){
		data.forEach(function(state){
			var l = map(state[sector], range[0], range[1], 95, 50);
			$("#"+state["state"]).css("fill","hsl(24,99%," + l + "%)");
		});
	}

	function getCountryNumbers(){
        var return_data = [0,0];
        number_creative_business.forEach(function(state) {
            return_data[0] += state.business_estimate;
            return_data[1] += state.employment_estimate;
        });

        return return_data;
    }
});