$(document).ready(function() {
	/*-----Fig 7---------------------*/

    var barChartData = {
        labels: ['Quintana Roo', 'Querétaro', 'Ciudad De México', 'Baja California Sur', 'Jalisco', 'Tabasco', 'Nuevo León', 'Aguascalientes', 'Campeche','Coahuila', 'Veracruz','Yucatán','Sonora','Oaxaca','Guanajuato','Sinaloa','Baja California','México','Tamaulipas','Guerrero','Nayarit','Chiapas','San Luis Potosí','Durango','Colima','Puebla','Hidalgo','Chihuahua','Morelos','Michoacán','Zacatecas','Tlaxcala'],
        datasets: [{
            label: 'Crecimiento en todos los sectores',
            backgroundColor: "#57bfb8",
            borderColor: "#57bfb8",
            borderWidth: 1,
            data: [
                8.598258315,
                5.331790411,
                4.302891364,
                8.969322858,
                3.620177542,
                5.013844937,
                4.84625151,
                3.391511305,
                3.925582824,
                3.55883234,
                2.510617507,
                2.692753901,
                3.584089323,
                4.92206066,
                2.242523289,
                3.887100076,
                5.883747684,
                2.076999926,
                3.229260014,
                1.763246842,
                6.8683565,
                1.336216308,
                2.703067071,
                2.023380562,
                2.829432969,
                1.85753346,
                1.444681139,
                3.425210763,
                2.137511226,
                1.422136479,
                1.733748085,
                0.921786168
            ]
        }, {
            label: 'Crecimiento creativo',
            backgroundColor: "#ffdd14",
            borderColor: "#ffdd14",
            borderWidth: 1,
            data: [
                10.0660066,
                9.953822473,
                7.316503039,
                5.962059621,
                5.38176427,
                5.183946488,
                4.709007486,
                4.525288376,
                4.50928382,
                3.604481247,
                3.010802114,
                2.67141585,
                2.582866982,
                2.465131366,
                2.069139541,
                1.772525849,
                1.698933228,
                1.594756963,
                1.361867704,
                1.201201201,
                1.160337553,
                0.991365526,
                0.892857143,
                0.769230769,
                0.769230769,
                0.25025025,
                -0.321027287,
                -0.721255834,
                -0.79787234,
                -1.104525862,
                -1.931518876,
                -3.130929791
            ]
        }]

    };

    var ctxFig7 = document.getElementById('fig7-canvas').getContext('2d');
    window.myBar = new Chart(ctxFig7, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            legend: {
                display: false
            },
            title: {
                display: false,
                text: 'Chart.js Bar Chart'
            },
            scales: {
                xAxes: [{
                   ticks: {
                    autoSkip: false,
                    maxRotation: 90,
                    minRotation: 90
                   },
                   gridLines: {
                        display:false
                    } 
                }]
            }
        }
    });

    /*------------------------------*/
});