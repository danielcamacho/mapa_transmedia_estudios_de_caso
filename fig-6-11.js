// Funcion reestructurada para colocar los colores de acuerdo a los valores en las matrices de circulos
// =====================================================================================================

function scale(num, in_min, in_max, out_min, out_max)
{
    if (out_min < 249)
        out_min += 360;
    if (out_max < 249)
        out_max += 360;
    val = (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    if (val > 359)
        val -=360;
    return val;
}


function getDataMatCirc(ev) {
    var x = ev.clientX;
    var y = ev.clientY;
    var target = $(ev.target);
    var val = target.attr('data-val');
    var color = target[0].style.backgroundColor;
    //console.log(x+', '+y);
    //console.log(val);
    //console.log(color);
    //poner div
    tooltipStyle = 'style="position: absolute; z-index:200; top: 0; right: 0; background: black; width: 130px; height: 30px; color: white; font-size: small; top: -95%; left: -130%; border-radius: 5px; padding: 6px; text-align: center;"'
    divToolStr = '<div class="circTooltip" '+tooltipStyle+'>';
    divToolStr += val;
    divToolStr += '</div>';
    //console.log(divToolStr);
    target.append(divToolStr);
}

function clearfigSeisTooltip()
{
    //console.log('cierra tooltip');
    $('.circTooltip').remove();
}

function getDataMatMiniCirc(ev) {
    var x = ev.clientX;
    var y = ev.clientY;
    var target = $(ev.target);
    var val = target.attr('data-val');
    var color = target[0].style.backgroundColor;
    //console.log(x+', '+y);
    //console.log(val);
    //console.log(color);
    //poner div
    tooltipStyle = 'style="position: absolute; z-index:200; top: 0; right: 0; background: black; width: 180px; height: 30px; color: white; font-size: small; top: -275%; left: -695%; border-radius: 5px; padding: 6px; text-align: center;"'
    divToolStr = '<div class="minicircTooltip" '+tooltipStyle+'>';
    divToolStr += val;
    divToolStr += '</div>';
    //console.log(divToolStr);
    target.append(divToolStr);
}

function clearfigOnceTooltip()
{
    //console.log('cierra tooltip');
    $('.minicircTooltip').remove();
}
// =====================================================================================================


$(document).ready(function() {    
	/*-------FIG 6------------------*/

    colocacion_sectores.forEach(function(sector){
        var row = "<div class='matrix-row'>";

        var style = "style='background: hsl("+scale(sector.advertising_marketing, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.advertising_marketing+"'></div>";
        var style = "style='background: hsl("+scale(sector.design, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.design+"'></div>";
        var style = "style='background: hsl("+scale(sector.software, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.software+"'></div>";
        var style = "style='background: hsl("+scale(sector.architecture, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.architecture+"'></div>";
        var style = "style='background: hsl("+scale(sector.music_performing_arts, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.music_performing_arts+"'></div>";
        var style = "style='background: hsl("+scale(sector.publishing, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.publishing+"'></div>";
        var style = "style='background: hsl("+scale(sector.film_radio_tv, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.film_radio_tv+"'></div>";
        var style = "style='background: hsl("+scale(sector.libraries_museums, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.libraries_museums+"'></div>";
        var style = "style='background: hsl("+scale(sector.crafts, -1, 1, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.crafts+"'></div>";


        row += "</div>";



        $("#colocation-matrix").append(row);
    });

    colocacion_sectores_right.forEach(function(sector){
        var row = "<div class='matrix-row'>";

        var style = "style='background: hsl("+scale(sector.advertising_marketing, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.advertising_marketing+"'></div>";
        var style = "style='background: hsl("+scale(sector.design, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.design+"'></div>";
        var style = "style='background: hsl("+scale(sector.software, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.software+"'></div>";
        var style = "style='background: hsl("+scale(sector.architecture, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.architecture+"'></div>";
        var style = "style='background: hsl("+scale(sector.music_performing_arts, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.music_performing_arts+"'></div>";
        var style = "style='background: hsl("+scale(sector.publishing, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.publishing+"'></div>";
        var style = "style='background: hsl("+scale(sector.film_radio_tv, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.film_radio_tv+"'></div>";
        var style = "style='background: hsl("+scale(sector.libraries_museums, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.libraries_museums+"'></div>";
        var style = "style='background: hsl("+scale(sector.crafts, -1, 1, 234, 100)+", 70%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatCirc(event)' onmouseout='clearfigSeisTooltip()' " + style + " data-val='"+sector.crafts+"'></div>";


        row += "</div>";



        $("#colocation-right-matrix").append(row);
    });


    $("#colocacion-sectores .option").click(function(){
        $("#colocacion-sectores .option").removeClass('active');
        $(this).addClass("active");

        $(".hide-f6").removeClass("active");
        $("#"+$(this).attr("data-target")).addClass("active");
    });

    /*------- END FIG 6------------------*/



    /*-------FIG 11------------------*/
        meetup_concurrency.forEach(function(sector){
        var row = "<div class='matrix-row'>";

        var style = "style='background: hsl("+scale(sector.desarrollo_web, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.desarrollo_web+"'></div>";
        var style = "style='background: hsl("+scale(sector.nuevas_tecnologias, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.nuevas_tecnologias+"'></div>";
        var style = "style='background: hsl("+scale(sector.programacion, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.programacion+"'></div>";
        var style = "style='background: hsl("+scale(sector.javascript, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.javascript+"'></div>";
        var style = "style='background: hsl("+scale(sector.startups, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.startups+"'></div>";
        var style = "style='background: hsl("+scale(sector.apps, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.apps+"'></div>";
        var style = "style='background: hsl("+scale(sector.datos_masivos, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.datos_masivos+"'></div>";
        var style = "style='background: hsl("+scale(sector.makers, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.makers+"'></div>";
        var style = "style='background: hsl("+scale(sector.marketing_digital, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.marketing_digital+"'></div>";
        var style = "style='background: hsl("+scale(sector.seo, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.seo+"'></div>";
        var style = "style='background: hsl("+scale(sector.colaboracion, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.colaboracion+"'></div>";
        var style = "style='background: hsl("+scale(sector.testing, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.testing+"'></div>";
        var style = "style='background: hsl("+scale(sector.mobil, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.mobil+"'></div>";
        var style = "style='background: hsl("+scale(sector.agile, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.agile+"'></div>";
        var style = "style='background: hsl("+scale(sector.administracion_sistemas, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.administracion_sistemas+"'></div>";
        var style = "style='background: hsl("+scale(sector.codigo_abierto, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.codigo_abierto+"'></div>";
        var style = "style='background: hsl("+scale(sector.php, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.php+"'></div>";
        var style = "style='background: hsl("+scale(sector.videojuegos, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.videojuegos+"'></div>";
        var style = "style='background: hsl("+scale(sector.usabilidad, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.usabilidad+"'></div>";
        var style = "style='background: hsl("+scale(sector.google, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.google+"'></div>";
        var style = "style='background: hsl("+scale(sector.grafico, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.grafico+"'></div>";
        var style = "style='background: hsl("+scale(sector.ai, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.ai+"'></div>";
        var style = "style='background: hsl("+scale(sector.wordpress, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.wordpress+"'></div>";
        var style = "style='background: hsl("+scale(sector.programacion_funcional, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.programacion_funcional+"'></div>";
        var style = "style='background: hsl("+scale(sector.microsoft, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.microsoft+"'></div>";
        var style = "style='background: hsl("+scale(sector.freelance, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.freelance+"'></div>";
        var style = "style='background: hsl("+scale(sector.vr_ar, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.vr_ar+"'></div>";
        var style = "style='background: hsl("+scale(sector.robots, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.robots+"'></div>";
        var style = "style='background: hsl("+scale(sector.contenido, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.contenido+"'></div>";
        var style = "style='background: hsl("+scale(sector.blogs, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.blogs+"'></div>";
        var style = "style='background: hsl("+scale(sector.business_networking, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.business_networking+"'></div>";
        var style = "style='background: hsl("+scale(sector.coaching, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.coaching+"'></div>";
        var style = "style='background: hsl("+scale(sector.crm, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.crm+"'></div>";
        var style = "style='background: hsl("+scale(sector.crypto, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.crypto+"'></div>";
        var style = "style='background: hsl("+scale(sector.emprendeduria, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.emprendeduria+"'></div>";
        var style = "style='background: hsl("+scale(sector.emprendeduria_social, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.emprendeduria_social+"'></div>";
        var style = "style='background: hsl("+scale(sector.estrategia, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.estrategia+"'></div>";
        var style = "style='background: hsl("+scale(sector.finanzas, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.finanzas+"'></div>";
        var style = "style='background: hsl("+scale(sector.ict, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.ict+"'></div>";
        var style = "style='background: hsl("+scale(sector.ingenieria, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.ingenieria+"'></div>";
        var style = "style='background: hsl("+scale(sector.inversion, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.inversion+"'></div>";
        var style = "style='background: hsl("+scale(sector.liderazgo, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.liderazgo+"'></div>";
        var style = "style='background: hsl("+scale(sector.motivacion, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.motivacion+"'></div>";
        var style = "style='background: hsl("+scale(sector.mujeres_emprendedoras, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.mujeres_emprendedoras+"'></div>";
        var style = "style='background: hsl("+scale(sector.networking, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.networking+"'></div>";
        var style = "style='background: hsl("+scale(sector.rh, 0, 0.24, 249, 50)+", 100%, 70%); position: relative;'";
        row += "<div class='matrix-circle' onmouseover='getDataMatMiniCirc(event)' onmouseout='clearfigOnceTooltip()' " + style + " data-val='"+sector.rh+"'></div>";

        row += "</div>";

        $("#meetup-concurrency-matrix").append(row);


    });
    
    /*-------FIG 11------------------*/

});