$(document).ready(function(){

    var svg = document.getElementById("blob1");
    var s = Snap(svg);

    var path = Snap.select("#path1");
    
    animatePath(); // start loop

    function animatePath(){
        path.animate({ d: "M1123.96,585.099c0,255.66-417.44,377.813-723.96,377.813C190.478,962.912,44.997,590.723,44.997,500C44.997,284.248,420.41,37.089,600,37.089C906.52,37.089,1123.96,329.44,1123.96,585.099z" }, 8000, mina.linear, resetPath);
    }

    function resetPath(){
        path.animate({ d: "M1155.003,500c0,255.66-448.483,462.912-755.003,462.912S44.997,755.66,44.997,500C44.997,244.342,293.48,37.089,600,37.089S1155.003,244.342,1155.003,500z" }, 8000, mina.linear, animatePath);
    }
});
