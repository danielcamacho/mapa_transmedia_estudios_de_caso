var current_section = "introduccion";
$(document).ready(function() {
    window.addEventListener('scroll', function(e) {

        $('.section').each(function(){
            if( isOnScreen( $(this) ) ) { 
                var section = $(this).attr("id");
                if(section != current_section){
                    $('.side-menu .item').removeClass('active');
                    $("[data-target*='"+section+"'").addClass("active");
                    current_section = section;
                }
            }  
        });
         
    });

    $('.mobile-menu').click(function(){
        $('.hamburger').toggleClass('active');
        $('.mobile-nav').toggleClass('active');
    });

    function isOnScreen(elem)
    {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + 200;
        var elemTop = $(elem).offset().top;
        return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
    }
});