$(document).ready(function() {
	    /*---- Size meetups----------*/

    var sizeMeetupsData = {
        labels: ['Otros sectores','Software','Diseño','Videojuegos, inmersión y apps','Publicidad y Marketing','Freelance'],
        datasets: [{
            label: 'Número de grupos',
            backgroundColor: "#6ee1a5",
            borderColor: "#6ee1a5",
            borderWidth: 1,
            data: [
                434,
                399,
                182,
                92,
                46,
                10
            ]
        }]

    };

    var ctxFig8 = document.getElementById('fig8-canvas').getContext('2d');
    window.myBar = new Chart(ctxFig8, {
        type: 'bar',
        data: sizeMeetupsData,
        options: {
            responsive: true,
            legend: {
                display: false
            },
            title: {
                display: false,
                text: 'Chart.js Bar Chart'
            },
            scales: {
                xAxes: [{
                   ticks: {
                    autoSkip: false,
                    minRotation: 0,
                    maxRotation: 0
                   },
                   gridLines: {
                        display:false
                    } 
                }]
            }
        }
    });


    /*----------------------------*/
});