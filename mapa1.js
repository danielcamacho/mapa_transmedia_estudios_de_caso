var selected_sector = "all_creative";
var data_set = concentration_creative_business;
var state_data, stateChart;

$(document).ready(function() {
	var range = getSectorRange(data_set, selected_sector);
    mapSector(data_set, selected_sector, range);

    getCountryAverage();

    $('.cbx').click(function(){
        if($(this).prop('checked')){
            selected_sector = $(this).attr('data-sector');
            var range = getSectorRange(data_set, selected_sector);
            mapSector(data_set, selected_sector, range);
        }
        $('.cbx').prop('checked',false);
        $(this).prop('checked',true);
    });
	$('.data-set').click(function(){
        $('.data-set').removeClass("active");
        $(this).addClass("active");

        var selected_data = $(this).attr("data-set");

        switch(selected_data){
            case "concentration_creative_business":
                data_set = concentration_creative_business;
                break;
            case "concentration_creative_employment":
                data_set = concentration_creative_employment;
                break;
        }
        
        var range = getSectorRange(data_set, selected_sector);

        mapSector(data_set, selected_sector, range);
    });

	function map (num, in_min, in_max, out_min, out_max) {
	  return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

    function getCountryAverage(){
        var return_data = [0,0,0,0,0,0,0,0,0];
        creative_specialisation_map.forEach(function(state){
            return_data[0] += state.advertising_marketing;
            return_data[1] += state.architecture;
            return_data[2] += state.crafts;
            return_data[3] += state.design;
            return_data[4] += state.film_radio_tv;
            return_data[5] += state.libraries_museums;
            return_data[6] += state.music_performing_arts;
            return_data[7] += state.publishing;
            return_data[8] += state.software;
        });
        for(var i=0; i<return_data.length; i++){
            return_data[i] = return_data[i]/32;
        }

        return return_data;
    }

    function getSectorRange(data, sector){
		var min = 999999,
		max = 0;

		data.forEach(function(state){
			if(state[sector]>max){
				max = state[sector];
			}
			if(state[sector]<min){
				min = state[sector];
			}
		});

		$('#top-data').text(max);
		$('#bottom-data').text(min);

		return [min, max];
	}

	function mapSector(data, sector, range){
		data.forEach(function(state){
			var l = map(state[sector], range[0], range[1], 95, 50);
			$("#"+state["state"]).css("fill","hsl(24,99%," + l + "%)");
		});
	}

    function getStateData(data, stateId){
        var return_data;
        data.forEach(function(state){
            if(state.state == stateId){
                return_data = [state.advertising_marketing, state.architecture, state.crafts, state.design, state.film_radio_tv, state.libraries_museums, state.music_performing_arts, state.publishing, state.software];
            }
        });

        return return_data;
    }

    function getStateNumbers(stateId){
        var stateInfo;
        number_creative_business.forEach(function(state) {
            if(state.state == stateId) {
                stateInfo = state;
            }
        });
        return stateInfo;
    }

});